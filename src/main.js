import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueParticles from 'vue-particles'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTwitter, faLinkedin, faGitlab } from '@fortawesome/free-brands-svg-icons'
import { faBars, faTimes} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
var VueScrollTo = require('vue-scrollto');
import vuescroll from 'vue-scroll'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

library.add(faTwitter);
library.add(faGitlab);
library.add(faLinkedin);
library.add(faBars);
library.add(faTimes);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueParticles);
Vue.use(VueScrollTo)
Vue.use(vuescroll)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
